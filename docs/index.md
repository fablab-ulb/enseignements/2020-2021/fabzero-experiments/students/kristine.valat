#Kristine VALAT

##Présentation

###Formation
Je suis en deuxième année d'école d'ingénieur à  l'ESIEE Paris dans la filière SEI (Systèmes Electroniques Intelligents). Je suis actuellement en Erasmus dans l'Université Libre de Bruxelles.

###Projets effectués
Durant mon cursus j'ai pu effectué différents projets:

* Assemblage et programmation d'un robot destiné aux combats : [_Sumobot_](https://esieespace.fr/sumobot/)
* Programmation d'un jeu d'aventure
* Simulation d'un trafic routier
* Création d'un jeu et d'une intelligence artificielle
* Création d'une application pour apprendre la musique : [_Sonn'ata_](https://play.google.com/store/apps/details?id=com.sonnata.sonnata&gl=BE)

###Centres d'intérêt
Mes centres d'intérêts sont:

* la planche à voile
* le théâtre
* la culture japonaise
* la cuisine
