/*
  Ce programme fait clignoter une LED
*/

const int LED = 2; //Initialisation de la LED sur le port 2

void setup() {
  // initialisation de la LED en tant que sortie
  pinMode(LED, OUTPUT);
}

// boucle qui sera exécutée (équivalent du while(1)) la boucle ne s'arrete pas tant que l'on appuie pas sur reset
void loop() {
  digitalWrite(LED, HIGH);   //Allume la LED
  delay(1000);               //Attend 1 000 ms soit 1 s
  digitalWrite(LED, LOW);    //Eteint la LED
  delay(1000);               //Attend 1 000 ms soit 1 s
}
