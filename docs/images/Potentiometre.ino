/*
 * Programme lisant le potentiomètre
 */

const int POTENTIO = A0;
int var = 0;

void setup() {
  Serial.begin(9600); //initialisaton du port série
  pinMode(POTENTIO,INPUT);
}

void loop() {
  var = analogRead(POTENTIO);
  Serial.println(var);
  delay(1000);
}
