/*
FILE   : flex1.scad

AUTHOR : Kristine VALAT <kristine.valat@edu.esiee.fr>

DATE   : 2021-03-08

LICENSE : Attribution 4.0 International [CC BY 4.0] (https://creativecommons.org/licenses/by/4.0/)

CONTRIBUTORS :
  - Doriane GALBEZ <doriane.galbez@edu.esiee.fr> : corrections de bug
*/

$fn= 50;
epsilon = 0.01;
//ce que l'on peut changer
    //pièce carré arrondie
depth = 0.7; //largeur 0.7
height = 0.6; //hauteur 0.6

    //trou de la pièce
bordure = 0.1; //espace entre la bordure et le trou 0.1
longueur = 0.8; // longueur entre 2 trous 0.8

    //barre du milieu
length_mil = 8; //8
depth_mil = 0.1; //0.1

//paramètres fixés
    //pièce carré arrondie
diametre = depth;
rayon = depth/2;
rayon2 = rayon-bordure; //rayon des trous
length = longueur + bordure *2 ; //longueur 0.8



difference(){
    // partie cube
    union(){
        //partie barre du milieu   
        translate([0,depth/2,height/2])     //on a centré le cube 
        cube([length_mil,depth_mil,height],center = true);
        
        // partie cube1
        translate([(length_mil/2 + diametre),depth/2,height/2])
        cube([longueur,depth,height],center =true);
        translate([length_mil/2+rayon,rayon,height/2]) // bordure *2 pour la confiance
        cylinder(height,rayon,rayon,center = true);
        translate([length_mil/2+rayon+longueur,depth/2,height/2])
        cylinder(height,rayon,rayon,center = true);
        
     
        
    // partie cube2
        translate([-(length_mil/2 + diametre),depth/2,height/2])
        cube([longueur,depth,height],center=true);
        translate([-(length_mil/2+rayon),depth/2,height/2])
        cylinder(height,rayon,rayon,center = true);
        translate([-(length_mil/2+rayon+longueur),depth/2,height/2])
        cylinder(height,rayon,rayon,center = true);
    }
    
    // partie trous
    translate([length_mil/2 + rayon,rayon,-epsilon/2])
    cylinder(height+epsilon,rayon2,rayon2);
    translate([length_mil/2 +rayon+longueur,rayon,-epsilon/2])
    cylinder(height+epsilon,rayon2,rayon2);

    translate([-(length_mil/2 + rayon),rayon,-epsilon/2])
    cylinder(height+epsilon,rayon2,rayon2);
    translate([-(length_mil/2 +rayon+longueur),rayon,-epsilon/2])
    cylinder(height+epsilon,rayon2,rayon2);
}


