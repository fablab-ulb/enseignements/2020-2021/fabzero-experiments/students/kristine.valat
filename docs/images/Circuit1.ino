/*
  FILE   : Circuit1.ino

  AUTHOR : Kristine VALAT <kristine.valat@edu.esiee.fr>

  DATE   : 2021-27-04

  LICENSE : Attribution 4.0 International [CC BY 4.0] (https://creativecommons.org/licenses/by/4.0/)
 * Programme lisant le potentiomètre
 */

const int POTENTIO = A0; //le potentiomètre est connecté sur le port A0 de la carte Arduino
const int LED = 2; //Initialisation de la LED sur le port 2
int var = 0;  //création d'un variable

void setup() {
  pinMode(POTENTIO,INPUT);   // initialisation du potentiometre en tant qu'entrée
  pinMode(LED, OUTPUT); // initialisation de la LED en tant que sortie
}

void loop() {
  digitalWrite(LED, HIGH);   //Allume la LED
  var = analogRead(POTENTIO); //lecture de la valeur sur le potentiomètre
  delay(var);               //Attend 1 000 ms soit 1 s
  digitalWrite(LED, LOW);    //Eteint la LED
  var = analogRead(POTENTIO); //lecture de la valeur sur le potentiomètre
  delay(var);               //Attend 1 000 ms soit 1 s
}
