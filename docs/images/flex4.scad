/*
FILE   : flex4.scad

AUTHOR : Doriane GALBEZ <doriane.galbez@edu.esiee.fr>

DATE   : 2021-03-08

LICENSE : Attribution 4.0 International [CC BY 4.0] (https://creativecommons.org/licenses/by/4.0/)
*/
$fn = 100;        //nombre de face des cylindres
th = 0.6;         //epaisseur de la pièce
width = 0.7;      //largeur
ext_r = width/2;  //rayon des cylindres exterieurs
diff = 0.1;       //epaisseur des bordures des parties 1 et 2
epsilon = 0.01;   //valeur permettant de realiser des creux legerement plus profond pour obtenir un affichage plus clair

//dimensions du fil (ensemble de 2 cylindres
rayon_f = 3;
width_f = 0.1;

//dimensions pour les trous
dist = 0.8; //distance entre les trous
N = 2;      //nombre de trous

union(){

//premiere partie
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([0,-ext_r,0])
        cube([dist*(N-1),width,th]);
        translate([dist*(N-1),0,0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //ensemble de N cylindres pour realiser les trous
        for (i=[0:N]){
            translate([dist*i,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        }
    }
}

//fil
translate([dist*(N-1)+ext_r,-rayon_f+width_f/2,0])  //decalage a l'extremite de la partie precedente
difference(){
    cylinder(th,rayon_f,rayon_f);
    translate([0,0,-epsilon/2])
    cylinder(th+epsilon,rayon_f-width_f,rayon_f-width_f);
    translate([-rayon_f,-rayon_f,-epsilon/2])
    cube([rayon_f,rayon_f*2,th+epsilon]);
    translate([-epsilon/2,-rayon_f,-epsilon/2])
    cube([rayon_f+epsilon,rayon_f,th+epsilon]);
}

//deuxieme partie
translate([dist*(N-1)+ext_r+rayon_f-width_f/2,-rayon_f-(dist*(N-1)+ext_r)+width_f/2,0])
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([-ext_r,0,0])
        cube([width,dist*(N-1),th]);
        translate([0,dist*(N-1),0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //ensemble de N cylindres pour realiser les trous
        for (i=[0:N]){
            translate([0,dist*i,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        }
    }
}

}