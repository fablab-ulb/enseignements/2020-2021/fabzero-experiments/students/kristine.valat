/*
FILE   : flex4.scad

AUTHOR : Doriane GALBEZ <doriane.galbez@edu.esiee.fr>

DATE   : 2021-03-08

LICENSE : Attribution 4.0 International [CC BY 4.0] (https://creativecommons.org/licenses/by/4.0/)
*/
$fn = 100;        //nombre de face des cylindres
th = 0.6;         //epaisseur de la pièce
width = 0.7;      //largeur
ext_r = width/2;  //rayon des cylindres exterieurs
diff = 0.1;       //epaisseur des bordures des parties 1 et 2
epsilon = 0.01;   //valeur permettant de realiser des creux legerement plus profond pour obtenir un affichage plus clair
theta = 45;       //angle d'inclinaison des parties sur les cotes

//dimensions de la partie centrale
length_c = 1.5;
delta_y = 0.1;
delta_x = 0.25;

//dimension de la partie "satellite"
length_s = 2;
width_s = 0.1;
N_s = 2;

//dimensions pour les trous
dist = 0.8; //distance entre les trous
N = 3;      //nombre de trous

union(){

//premiere partie en angle
translate([0,-(cos(theta)*(dist*(N-1)+ext_r))-delta_y,0])
rotate([0,0,theta])
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([0,-ext_r,0])
        cube([dist*(N-1),width,th]);
        translate([dist*(N-1),0,0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //ensemble de N cylindres pour realiser les trous
        for (i=[0:N]){
            translate([dist*i,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        }
    }
}

//partie centrale
translate([sin(theta)*(dist*(N-1)+ext_r)-delta_x,-ext_r,0])  //decalage a l'extremite de la partie precedente
difference(){
    union(){
        cylinder(th,ext_r,ext_r);
        translate([0,-ext_r,0])
        cube([length_c+ext_r*2-diff,width,th]);
        translate([length_c+ext_r*2-diff,0,0]) //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);
    }
    union(){ //creux
        translate([0,0,-epsilon/2])
        cylinder(th+epsilon,ext_r-diff,ext_r-diff); //cylindre a une extremite

        translate([ext_r*2-diff/2,0,0])
        union(){
            translate([0,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff); //cylindre pour arrondir le bord interieur

            translate([0,-ext_r+diff,-epsilon/2])
            cube([length_c-ext_r*2,width-diff*2,th+epsilon]);

            translate([length_c-ext_r*2,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff); //cylindre pour arrondir le bord interieur
        }

        translate([length_c+ext_r*2-diff,0,-epsilon/2])
        cylinder(th+epsilon,ext_r-diff,ext_r-diff); //cynlindre a une extremite
    }
}


//deuxieme partie en angle
translate([sin(theta)*(dist*(N-1)+ext_r)+(length_c+ext_r*2-diff)-delta_x,-ext_r,0])
rotate([0,0,-theta])
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([0,-ext_r,0])
        cube([dist*(N-1),width,th]);
        translate([dist*(N-1),0,0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //ensemble de N cylindres pour realiser les trous
        for (i=[0:N]){
            translate([dist*i,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        }
    }
}


//partie "satellite"
translate([sin(theta)*(dist*(N-1)+ext_r)-delta_x+(length_c+ext_r*2-diff)/2+width_s/2,-(dist*(N_s-1)+ext_r+length_s+width),0])
rotate([0,0,90])
union(){
    difference(){
        union(){
            cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
            translate([0,-ext_r,0])
            cube([dist*(N_s-1),width,th]);
            translate([dist*(N_s-1),0,0])    //decalage a l'extremite du pave
            cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        }
        union(){  //ensemble de N cylindres pour realiser les trous
            for (i=[0:N_s]){
                translate([dist*i,0,-epsilon/2])
                cylinder(th+epsilon,ext_r-diff,ext_r-diff);
            }
        }
        }

        //fil
        translate([dist*(N_s-1)+ext_r,-width_s/2,0])  //decalage a l'extremite de la partie precedente
        cube([length_s,width_s,th]);
    }

}