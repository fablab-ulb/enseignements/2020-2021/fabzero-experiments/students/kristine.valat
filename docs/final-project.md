# Final Project
Ce projet a été fait en collaboration avec[Doriane Galbez](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/).
Le but de se projet est de réunir les étudiants qui le souhaite dans un groupe de cuisine afin de faire des plats équilibrés pour la semaine. L'insciption au groupe se fera grâce à un site, les participants choisiront les recettes à faire pour la semaine et cuisineront chez un des particpants qui a une cuisine qui a assez de place pour les accueillir. Pour donner envie aux personnes de rejoindre ce groupe nous proposons de laisser des biscuits sains avec des logos dessinés dessus, les logos seront dessinés à l'aide d'une sorte d'emporte pièce. Les logos sont facilement réalisable à l'aide d'une découpeuse laser ce qui permmettra d'en faire plusieurs. Nous avons appelé ce projet All for cook !.
![](./images/logo_all_for_cook.png)

##Choix du sujet
Le choix de ce sujet s'est fait en plusieurs étapes. Tout d'abord, nous nous sommes concentrées sur le thème de la cuisine. C'est un sujet qui me parle tout particulièrement puisque j'aime bien cuisiner et surtout cuisiner avec les autres et pour les autres. En effet, je vois la cuisine comme un moment d'échange et de partage.
Nous nous sommes donc posées la question **"Comment aider les étudiants à mieux manger ?"**.

Nous avons donc eu plusieurs pistes qui ont menée sur plusieurs idées pour répondre à cela :
- Dans un premier temps, nous souhaitions faire un projet qui puisse aider les étudiants à cuisiner plus facilement et qu'ils puissent cuisiner des plats plus sains/équilibrés. Pour cela nous avons eu les idées suivantes :
  - créer un four démontable/petit (afin de permettre aux étudiants qui n'ont pas de four et pas de place de leur permettre d'avoir un four). *Pour répondre à ce problème, nous avons vu qu'il était possible de faire l'effet d'un four en utilisant une poele*.
  - créer un plan de travail supplémentaire facilement rangeable (afin que les étudiants aient plus de place pour cuisiner puisque beaucoup d'étudiants n'ont pas beaucoup de place pour cuisiner). *Cette idée n'a pas aboutie car elle ne contenait pas assez d'informations techniques*.
  - créer des outils pour la cuisine low cost (par exemple un fouet électrique) pour à long terme créer une panoplie pour les étudiants avec tout le nécessaires pour faire des plats plus élaborés. *Pour répondre à ce problème, nous avons vu qu'il existait déjà des outils low-cost*.

- Dans un deuxième temps, nous souhaitions faire un projet qui puisse aider les étudiants à mieux gérer leur repas équilibrés en gérant les proportions dans leurs assiettes (quantité de féculents, légumes et protéines). Pour cela nous avons eu les idées suivantes :
  - créer un outil type emporte-pièce pour répartir les proportions dans les bonnes quantités. *Pour répondre à ce problème, nous avons vu qu'il était difficile de le faire à l'imprimante 3D dû au fait que lors de l'impression l'imprimante 3D fait de nombreuses micro-fissures et il faut donc recouvrir le produit avec une résine spéciale pour le contact alimentaire. De plus, il est aussi possible d'imprimer avec l'imprimante avec un fil qui permet le contact alimentaire (même si cela ne règle pas le problème des micro-fissures).*.
  - créer une assiette modulable avec chaque partie qui correspondrait à un type (légumes, protéines ou féculents). *Même problème que l'idée précédente*.

- Dans un troisième temps, nous avons discuter avec plusieurs étudiants afin de comprendre pourquoi ils ne cuisinaient pas/peu ou pas/peu de repas équilibrés. Nous avons donc cherché un moyen pour aider les étudiants sur le problème de la motivation pour cuisiner (principalement due au temps et à la solitude). Pour cela nous avons eu les idées suivantes :
  - créer un groupe via un site internet pour rassembler les personnes pour qu’ils puissent cuisiner leurs plats pour toute une semaine à plusieurs. **Nous nous sommes concentrées sur ce dernier point, notamment sur la communication de ce projet et comment donner envie aux étudiants de rejoindre le groupe de cuisine ainsi que le moyen de s'inscrire dans le groupe**.


## Inscription dans les objectifs du développement durable

Nous avond dû inscrire notre projet dans au moins un des objectifs du développement durable ([disponibles ici](https://sdgs.un.org/)).
Nous pensons que notre projet s'incrit dans 2 objectifs :
- Bonne santé et bien-être. En effet, notre projet permettrait aux étudiants de cuisiner des plats plus équilibrés et donc d'être en meilleure santé. De plus, cela permettrait aussi un contact social qui réduirait la solitude pour les étudiants.
- Réduire les inégalité. En effet, les étudiants ont parfois peu de budget, pas/peu d'accès à du matériel pour cuisiner. Cela leur permettrait d'avoir un budget plus adapté pour cuisiner ainsi que du matériel nécessaire pour cuisiner.

## Aspect scientifique
Pour l'aspect scientifique, nous avons efffectué un sondage et fait plusieurs recherches sur comment manger sainement ainsi que les recettes pour les biscuits que nous proposeront afin d'en faire les plus sains possibles.

###Sondage
Nous avons effectué un sondage auprès des étudiants et d'anciens étudiants afin d'avoir leur avis ainsi que leurs besoins [lien du questionnaire](https://docs.google.com/forms/d/1P14jox8yGBuVgEPAVeHW_RNoyC5Z7MC6pW3-wbW72o8/edit#responses). Nous obtenus les résultats suivants:

![](./images/sondage.PNG)
![](./images/sondage1.PNG)
![](./images/sondage2.PNG)
![](./images/sondage3.PNG)
![](./images/sondage4.PNG)
![](./images/sondage5.PNG)
![](./images/sondage6.PNG)
![](./images/sondage7.PNG)

Nous pouvons remarquer que 38% des étudiants intérrogés vivent seuls (ou vivaient seuls lorsqu'ils étaient étudiants). Nous remarquons aussi que le nombre d'entre eux cuisinent des plats simples (qui sont rarement équilibrés) est le même. De plus, seulement 22% mangent des assiettes équilibrées. 25% des interrogés sont prêts à rejoindre un groupe de cuisine afin de préparer des repas pour la semaine.  
Nous aussi posé la question de la raison pour laquelle ces personnes cuisinent et nous pouvons catégoriser les réponses entre ceux cuisinent et ceux qui ne le font pas/peu :
- Ceux qui ne cuisinent pas/peu
>"Manque de temps et je rentre chez mes parents le week end"
"En rentrant fatigué pas trop le temps"
"J'ai pas le temps et je ne sais pas bien cuisiner j'ai pas l'habitude)."
"Peu de temps"
"Je travaillais en même temps et j'avais le temps de rien faire, donc les pâtes (les moin cher bien entendu) était la facilité"
"Parce que j’ai pas le temps"

>"Je mange au RU en semaine et le week-end j'aime cuisiner mais je commande/fais des trucs très simple par flemme assez souvent"
"car commander c'est la facilité"

**Nous pouvons remarquer 2 raisons pour lesquelles les étudiants ne cuisinent pas le manque de temps et le manque de motivation. Cela coïncide avec les discussions que nous avons eu au préalable avec d'autres étudiants.**

- Ceux qui cuisinent
>"Aucune restauration pas chère aux alentours à cause du covid, impossible d'aller au RU"
"Je n'ai pas beaucoup d'argent pour commander donc j'essaie de cuisiner moi-même."
"Pas les moyens de commander ou de sortir manger"
"C'est moins cher à long terme de cuisiner plutôt que de commander (ou plats préparés)"

>"J'aime cuisiner, c'est moins cher et ça me permet de décompresser"
"faire à manger me détend et cela est beaucoup plus sain que les plats tout fait"
Je cuisine parce que sa me plait et que je peux manger ce que je desir

>Je fais de la musculation donc je me prépare des dingueries culinaires équilibrées et incroyablement délicieuses tous les jours"

>"Rapide et en grande quantité"
"Je fais des grosses quantites pour gagner du temps et avoir de quoi manger pour plusieurs repas."

>"Par flemme. Parfois je préfère sauter un repas plutôt que de faire la vaisselle (ou alors je fais la vaisselle uniquement quand je meurt de faim pour réutiliser une casserole). En revanche quand j'ai habité chez mon copain pendant les un an de confinement c'était beaucoup plus motivant de lui préparer de bons petits plats et de les partager ensemble (d'autant quil a un lave vaisselle )."

**Nous pouvons remarquer plusieurs raisons pour lesquelles les étudiants cuisinent car cela permet coûte moins cher, c'est possible de faire des grandes quantités, cela détend et cela peut leur permettre de manger plus équilibré. De plus, dans le dernier commentaire, nous pouvons remarquer que la personne trouve ca plus motivant de cuisiner pour quelqu'un et de partager le repas avec cette personne**


###Recherches
"Que veut dire bien manger ?" C'est la principale question que nous nous sommes posées durant ce projet. En effet, il est simple de dire qu'il faut bien manger et manger équilibré mais nous avons du mal à savoir pourquoi ou comment le faire. Nous avons trouvé un article de l'université d'Harvard T. H. Chan School of Public Health’s (département nutrition) qui nous explique ce que doit contenir une assiette équilibrée. ([Lien de l'article en anglais](https://www.hsph.harvard.edu/nutritionsource/healthy-eating-plate/) , [Lien de l'article en français](https://www.hsph.harvard.edu/nutritionsource/healthy-eating-plate/translations/french_canada/))

Dans cet article, nous pouvons voir ce qui entendu quand on parle d'assiette saine pour faire un plat équilibré. Nous observons donc que l'assiettes est divisée en 3 parties, la moitié doit être composée de fruits et de légumes, un quart doit être des cérales complètes et le dernier quart doit être des protéines (animales ou végétales). De plus, à cela nous pouvons ajouter des huiles santés pour accompagner le plat ou la cuisson et il faut boire de l'eau, du thé ou du café. Ils nous informent aussi qu'il faut prendre 1 à 2 portions de produits laitiers par jour (c'est-à-dire 1 à 2 verres de lait /yaourts / tranches de fromage).

![](./images/assiette_saine.PNG)

Nous avons trouvé sur un site fait par un professionel de santé plus de détail sur les légumes, protéines, ceréales complètes, bonnes graisses, épices et aromatiques et fruits qu'il faut favoriser pour repas équilibré. ([Voici le lien du site si cela vous intéresse](https://toutpourmasante.fr/composer-repas-equilibre/)).

De plus, nous avons effectué des recherches pour les biscuits que nous souhaitions préparer, nous cherchions un biscuit qui serait peu sucré pour que cela soit le plus sain possible car le sucre n'est pas bon pour la santé et peut entraîner de nombreuses maladies telles que diabète de type 2, des maladies cardiovasculaires et certains cancers. [Source ANSES](https://www.anses.fr/fr/content/sucres-dans-l%E2%80%99alimentation)

Il y a beaucoup de risque d'allergies alimentaires donc nous avons essayer de réduire les risques au maximum.

![](./images/allergies.PNG)
[Source Le Monde qui a repris les résultats de l'ANSES](https://www.lemonde.fr/planete/article/2018/12/20/qu-est-ce-qu-on-mange-l-assiette-des-francais-decortiquee_5400180_3244.html)

Nous avons évité les recettes avec du lait, des fruits à coques et des arachides.

Nous alors trouvé cette [recette](https://www.marmiton.org/recettes/recette_cookies-sans-sucre-sans-beurre-a-basse-calories_347957.aspx) qui permet de faire des biscuits avec les ingrédients suivants:
- farine
- chocolat noir (car moins sucré que le chocolat au lait)
- bicarbonate (nous l'avons remplacé par de la levure puisque l'effet est le même)
- vanille
- oeuf
- crème
- compote (nous avons utilisée une allégée en sucre)



## Utilisation des outils du Fablab
Découpeuse laser pour faire un “moule / emporte-pièce” afin de dessiner un logo sur les biscuits pour les rendre plus attractifs.

L'étape la plus délicate dans notre projet c'est de le faire connaître. Nous avons pensé que nous pourrions utiliser un objet concret pour interpeller les gens et leur donner envie de se renseigner sur le projet. Et puisque nous parlons de cuisine, pourquoi ne pas en profiter pour que cet objet soit des biscuits !

Notre idée est de faire des biscuits avec un minimum de sucre, de rendre ce biscuit attractif et de placer à côté une petite pancarte expliquant le principe du site. Les biscuits seront préparés par les membres du groupe lorsqu’ils se réunissent pour cuisiner leurs plats. Pour que ces biscuits soient attractifs nous souhaitons utiliser des décorations qui pourront varier régulièrement. Une idée que nous avons eu est de créer une sorte de petit moule découpable à la laser pour dessiner la forme que l’on veut en chocolat (noire pour avoir moins de sucre et parce que le chocolat noire contient du magnésium qui est bon pour éviter les crampes par exemple). Ainsi, les membres du groupe qui ont accès au FabLab pourront chaque semaine découper une nouvelle forme (suivant leur envie) et les biscuits auront régulièrement de nouvelles décorations. De plus, préparer des biscuits et les décorer pour les partager ensuite peut être assez motivant pour rejoindre le groupe de cuisine. Sur le site nous souhaitons mettre une partie tutoriel pour montrer comment découper son image pour créer le moule.

_Remarque : Les moules peuvent être réutilisés d’une semaine à l’autre il n’y a pas d’obligation de les changer toutes les semaines._

### Nos exemples

Pour commencer et faire les premiers biscuits, nous avons créé deux moules : le premier dessine un smiley et le deuxième le logo du FabLab.

Pour créer le smiley, nous avons utilisé Inkscape pour directement le dessiner. Pour cela, nous avons utilisé les outils présents sur le côté gauche.


![](./images/inkscape_smiley.png)

> Attention : Pour pouvoir découper en vecteur il faut que les contours aient une épaisseur de 0,01 mm.


![](./images/inkscape_smiley1.PNG)


Ensuite, nous avons découpé notre pièce. Pour trouver les paramètres de la découpe que nous devions utiliser pour du plexiglas nous avons utilisé une grille de calibration disponible à côté de la découpeuse laser. Pour faire une découpe (et non juste graver) nous avons choisi de mettre 10% en vitesse, 55% de puissance et 100% en fréquence.

![](./images/decoupe_laser_smiley.png)



Pour le logo du FabLab, nous sommes parties d’une image SVG du logo que nous avons ouverte sous Inkscape.

![](./images/logo_fablab.PNG)

De cette image nous avons gardé uniquement le logo.

Ensuite, nous avons effacé le fond et définit les contours en noir. Pour cela, nous avons sélectionné l’image puis nous avons pris une couleur du petit bandeau en bas que nous avon fait glissé dans les cases “Fond” et “Contour”. Pour le fond nous avons l’absence de couleur (la case avec la croix rouge) et pour le contour nous avons pris la couleur noir.

![](./images/inkscape_fablab_logo_contours1.png)
![](./images/inkscape_fablab_logo_contours2.png)


Puis pour la découpe nous avons réalisé la même manipulation que précédemment avec le smiley.

Nous avons finalement obtenu les deux pièces suivantes :


![](./images/moules1.PNG)

##Réalisation
Nous avons fonctionné par étapes et d'abord réalisé les biscuits afin de les gouter avant. Voici le résultat obtenu :
![](./images/biscuits.PNG)

Je n'ai pas pour le moment l'image du biscuit final.
