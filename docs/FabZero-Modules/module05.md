# 5. Electronique 1 - Prototypage
Nous avons utilisé des composants ainsi qu'une carte Arduino Uno. De plus, nous avons réalisé des programmes afin de concevoir des circuits.

##Matériel utilisé
* Carte Arduino Uno
* LED rouge
* Potentiomètre
* Afficheur 7-segments
* Board
* Interface de développement : Arduino IDE

##Méthodologie
Avant de commencer à concevoir un circuit, je reagarde d'abord les composants que je vais utiliser. Puis, je regarde les datasheets (document qui décrit les composants) afin de connaître comment le composant électronique fonctionne. Cela permet aussi de connaître comment brancher ou relier le composant au circuit etde savoir l'éventuelle tension pour que le composant fonctionne. De plus, cela permet de savoir s'il faut rajouter une résistance avant de brancher le composant pour éviter de faire un dysfonctionnement.

Ensuite, je teste les composants un par un. Je branche le composant et je fais un programme simple afin de vérifier que le composant est fonctionnel et que j'ai bien compris comment l'utiliser avec un programme.
Si on ne fait pas ce test et que l'on fait le circuit directement et que l'on programme, s'il y a un problème on ne sait pas d'où il vient et c'est assez compliqué de trouver le problème. De plus, on ne sait pas si le problème vient d'un composant ou du programme.

##Premier programme : Clignotemment d'une LED
Cette première étape permet de faire clignoter une LED. C'est un programme basique et il est même proposé par l'interface de développement. Le programme a été commenté pour qu'il soit modifiable et que quelqu'un puisse le comprendre facilement.

Voici le branchement que j'ai utilisé pour ce premier circuit.

![](../images/board1.JPG)

Comme on peut le voir ci-dessus, la LED a 2 connexions une à la masse (le fil noir) et une au port 2 (le fil orange).

```
/*
  FILE   : Clignotement_LED.ino

  AUTHOR : Kristine VALAT <kristine.valat@edu.esiee.fr>

  DATE   : 2021-27-04

  LICENSE : Attribution 4.0 International [CC BY 4.0] (https://creativecommons.org/licenses/by/4.0/)

  Ce programme fait clignoter une LED
*/

const int LED = 2; //Initialisation de la LED sur le port 2

void setup() {
  // initialisation de la LED en tant que sortie
  pinMode(LED, OUTPUT);
}

// boucle qui sera exécutée (équivalent du while(1)) la boucle ne s'arrete pas tant que l'on appuie pas sur reset
void loop() {
  digitalWrite(LED, HIGH);   //Allume la LED
  delay(1000);               //Attend 1 000 ms soit 1 s
  digitalWrite(LED, LOW);    //Eteint la LED
  delay(1000);               //Attend 1 000 ms soit 1 s
}

```
[Voici le programme](../images/Clignotement_LED.ino)

##Deuxième programme : Lire les valeurs d'un potentiomètre
Cette deuxième étape permet de lire les valeurs d'un potentiomètre. C'est un programme basique et qui permet de vérifier le bon fonctionnement du composant. Le programme a été commenté pour qu'il soit modifiable et que quelqu'un puisse le comprendre facilement.

![](../images/board12.JPG)

Comme on peut le voir sur la photo le potentiomètre a 3 "connexions" : le fil noir représente la masse, le fil orange la donnée que le potentiomètre transmet (connectée au port A0) et le fil jaune l'alimentation pour que le potentiomètre fonctionne.

```
/*
  FILE   : Circuit1.ino

  AUTHOR : Kristine VALAT <kristine.valat@edu.esiee.fr>

  DATE   : 2021-27-04

  LICENSE : Attribution 4.0 International [CC BY 4.0] (https://creativecommons.org/licenses/by/4.0/)
 * Programme faisant clignoter la LED en fonction des valeurs du potentiomètre
 */

const int POTENTIO = A0; //le potentiomètre est connecté sur le port A0 de la carte Arduino
int var = 0;  //création d'un variable

void setup() {
  Serial.begin(9600); //initialisaton du port série
  pinMode(POTENTIO,INPUT);   // initialisation du potentiometre en tant qu'entrée
}

void loop() {
  var = analogRead(POTENTIO); //lecture de la valeur sur le potentiomètre
  Serial.println(var); // affichage de la valeur du potentiomètre dans un terminal de l'interface Arduino
  delay(1000);  //Attend 1 000s
}

```

[Voici le programme](../images/Potentiometre.ino)

Voici un exemple des valeurs affichées lorsque l'on fait varier le potentiomètre.

![](../images/potentio.PNG)

##Troisième programme : faire varier la LED en fonction des valeurs du potentiomètre
Ce programme relie les deux derniers et permet de faire varier le temps de clignotement de la LED en  fonction de la valeur du potentiomètre. Le programme a été commenté pour qu'il soit modifiable et que quelqu'un puisse le comprendre facilement.

![](../images/Circuit1.PNG)

```
/*
  FILE   : Circuit1.ino

  AUTHOR : Kristine VALAT <kristine.valat@edu.esiee.fr>

  DATE   : 2021-27-04

  LICENSE : Attribution 4.0 International [CC BY 4.0] (https://creativecommons.org/licenses/by/4.0/)
 * Programme lisant le potentiomètre
 */

const int POTENTIO = A0; //le potentiomètre est connecté sur le port A0 de la carte Arduino
const int LED = 2; //Initialisation de la LED sur le port 2
int var = 0;  //création d'un variable

void setup() {
  pinMode(POTENTIO,INPUT);   // initialisation du potentiometre en tant qu'entrée
  pinMode(LED, OUTPUT); // initialisation de la LED en tant que sortie
}

void loop() {
  digitalWrite(LED, HIGH);   //Allume la LED
  var = analogRead(POTENTIO); //lecture de la valeur sur le potentiomètre
  delay(var);               //Attend 1 000 ms soit 1 s
  digitalWrite(LED, LOW);    //Eteint la LED
  var = analogRead(POTENTIO); //lecture de la valeur sur le potentiomètre
  delay(var);               //Attend 1 000 ms soit 1 s
}

```

[Voici le programme](../images/Circuit1.ino)


##Quatrième programme : faire un serpent sur un afficheur 7-segments
Ce programme a été écrit en collaboration avec **Doriane Galbez**.
Cette quatrième étape permet d'afficher un serpent sur un afficheur 7-segments.

Voici le fonctionnement d'un afficheur 7-segment :
![](../images/schema_afficheurs_7seg.JPG)
Un "digit" correspond à ce qui est à gauche de la photo. On voir qu'il y en a quatre sur cet afficheur.

Afin d'utiliser l'afficheur 7-segments, nous le banchons de la manière suivante :
![](../images/schema_montage_7seg1.JPG)

Voici donc les branchements sur la carte :
![](../images/board2.JPG)

Nous avons allumé le milieu de l'afficheur pour vérifier son bon fonctionnement.
![](../images/board22.JPG)

Nous avons d'abord fait un premier programme qui permet d'alterner le milieu des afficheurs 7-segments qui nous permis de vérifier le bon fonctionnement de l'afficheur.
![](../images/board23.JPG)

```
/*
 *  File    : prog2_7seg.ino
 *  Authors : Doriane Galbez et Kristine Valat
 *  Date    : 11/03/2021
 *  License : This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * -------------------------------------------
 * Programme pilotant 4 afficheurs 7 segments.
 * -------------------------------------------
 *
 * Sur un afficheur 7 segments, les segments sont nommes de la maniere suivante :
 *       A
 *      ---
 *   F |   | B
 *     | G |
 *      ---
 *   E |   | C
 *     |   |
 *      ---  O DP
 *       D
 *
 *
 */


//definition des noms attribues aux ports
const int SEG_A = 12;    //segment correspondant au segment a
const int SEG_B = 8;     //segment correspondant au segment b
const int SEG_C = 4;     //segment correspondant au segment c
const int SEG_D = 5;     //segment correspondant au segment d
const int SEG_E = 6;     //segment correspondant au segment e
const int SEG_F = 11;    //segment correspondant au segment f
const int SEG_G = 3;     //segment correspondant au segment g
const int SEG_DP = 7;    //segment correspondant au segment du point
const int DIGIT_1 = 13;  //7 segment numero 1
const int DIGIT_2 = 10;  //7 segment numero 2
const int DIGIT_3 = 9;   //7 segment numero 3
const int DIGIT_4 = 2;   //7 segment numero 4


void setup() {
  //initialisation des ports
  pinMode(SEG_A,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(SEG_B,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(SEG_C,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(SEG_D,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(SEG_E,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(SEG_F,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(SEG_G,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(DIGIT_1,OUTPUT); //initialisation du port en tant que sortie
  pinMode(DIGIT_2,OUTPUT); //initialisation du port en tant que sortie
  pinMode(DIGIT_3,OUTPUT); //initialisation du port en tant que sortie
  pinMode(DIGIT_4,OUTPUT); //initialisation du port en tant que sortie

}

void loop() {
  /*serpent(500);*/

  //ici, les segments g d'un afficheur sur 2 clignotent avec une periode de 1 seconde
  eteint_tout();  //on eteint tout pour eviter que les segments precedemment allumes restent allumes
  affiche_un_seg(DIGIT_1,SEG_G);  //allume le segment g du 7 segment 1
  affiche_un_seg(DIGIT_3,SEG_G);  //allume le segment g du 7 segment 3
  delay(1000);    //attente de 1000 millisecondes soit 1 seconde
  eteint_tout();  //on eteint tout pour eviter que les segments precedemment allumes restent allumes
  affiche_un_seg(DIGIT_2,SEG_G);  //allume le segment g du 7 segment 2
  affiche_un_seg(DIGIT_4,SEG_G);  //allume le segment g du 7 segment 4
  delay(1000);    //attente de 1000 millisecondes soit 1 seconde

}

/*
 * Remise a zero de tout les segments
 */
void eteint_tout(){
  /* option 1*/
  digitalWrite(DIGIT_1, LOW);
  digitalWrite(DIGIT_2, LOW);
  digitalWrite(DIGIT_3, LOW);
  digitalWrite(DIGIT_4, LOW);

  /* option 2*/
  /*
  digitalWrite(DIGIT_1, HIGH);
  digitalWrite(DIGIT_2, HIGH);
  digitalWrite(DIGIT_3, HIGH);
  digitalWrite(DIGIT_4, HIGH);
  digitalWrite(SEG_A, LOW);
  digitalWrite(SEG_B, LOW);
  digitalWrite(SEG_C, LOW);
  digitalWrite(SEG_D, LOW);
  digitalWrite(SEG_E, LOW);
  digitalWrite(SEG_F, LOW);
  digitalWrite(SEG_G, LOW);
  digitalWrite(SEG_DP, LOW);
  */
}

/*
 * Eteint un segment sur un des afficheur 7 segments
 * - digit : (DIGIT_1,DIGIT_2,DIGIT_3 ou DIGIT_4) afficheur 7 segments qu'on veut utiliser
 * - seg   : (SEG_A,SEG_B,SEG_C,SEG_D,SEG_E,SEG_F,SEG_G ou SEG_DP) segment qu'on veut eteindre
 */
void eteint_un_seg(int digit, int seg){
  digitalWrite(digit, HIGH); //selection du 7 segments
  digitalWrite(seg, LOW);    //eteint un segment du 7 segments
}

/*
 * Allume un segment sur un des afficheur 7 segments
 * - digit : (DIGIT_1,DIGIT_2,DIGIT_3 ou DIGIT_4) afficheur 7 segments qu'on veut utiliser
 * - seg   : (SEG_A,SEG_B,SEG_C,SEG_D,SEG_E,SEG_F,SEG_G ou SEG_DP) segment qu'on veut allumer
 */
void affiche_un_seg(int digit, int seg){
  digitalWrite(digit, HIGH); //selection du 7 segments
  digitalWrite(seg, HIGH);   //allume un segment du 7 segments
}

/*
 * Definit un ensemble d'etapes representant un serpent qui avance sur les segments des afficheurs.
 * etape 1    etape 2    etape 3    etape 4    etape 5    etape 6    etape 7
 *    _         _             _           _          
 *   |         |          _  |        _  |         _           _
 *   |        _|           |_|      _| |_|       _| |_       _| |         _|
 */
void serpent_etape(int etape){
  eteint_tout();  //on eteint tout pour eviter que les segments precedemment allumes restent allumes
  switch(etape){
    default: //tous les 7 segments sont eteints
      break;
    case 1:
      affiche_un_seg(DIGIT_1,SEG_A);
      affiche_un_seg(DIGIT_1,SEG_F);
      affiche_un_seg(DIGIT_1,SEG_E);
      break;
    case 2:
      //7 segments 1
      affiche_un_seg(DIGIT_1,SEG_D);
      //7 segments 2
      affiche_un_seg(DIGIT_2,SEG_A);
      affiche_un_seg(DIGIT_2,SEG_F);
      affiche_un_seg(DIGIT_2,SEG_E);
      break;
    case 3:
      //7 segments 1
      affiche_un_seg(DIGIT_1,SEG_C);
      affiche_un_seg(DIGIT_1,SEG_G);
      //7 segments 2
      affiche_un_seg(DIGIT_2,SEG_D);
      //7 segments 3
      affiche_un_seg(DIGIT_3,SEG_A);
      affiche_un_seg(DIGIT_3,SEG_F);
      affiche_un_seg(DIGIT_3,SEG_E);
      break;
    case 4:
      //7 segments 1
      affiche_un_seg(DIGIT_1,SEG_D);
      //7 segments 2
      affiche_un_seg(DIGIT_2,SEG_C);
      affiche_un_seg(DIGIT_2,SEG_G);
      //7 segments 3
      affiche_un_seg(DIGIT_3,SEG_D);
      //7 segments 4
      affiche_un_seg(DIGIT_4,SEG_A);
      affiche_un_seg(DIGIT_4,SEG_F);
      affiche_un_seg(DIGIT_4,SEG_E);
      break;
    case 5:
      //7 segments 1 : eteint
      //7 segments 2
      affiche_un_seg(DIGIT_2,SEG_D);
      //7 segments 3
      affiche_un_seg(DIGIT_3,SEG_C);
      affiche_un_seg(DIGIT_3,SEG_G);
      //7 segments 4
      affiche_un_seg(DIGIT_4,SEG_D);
      break;
    case 6:
      //7 segments 1 : eteint
      //7 segments 2 : eteint
      //7 segments 3
      affiche_un_seg(DIGIT_3,SEG_D);
      //7 segments 4
      affiche_un_seg(DIGIT_4,SEG_C);
      affiche_un_seg(DIGIT_4,SEG_G);
      break;
    case 7:
      //7 segments 1 : eteint
      //7 segments 2 : eteint
      //7 segments 3 : eteint
      //7 segments 4
      affiche_un_seg(DIGIT_4,SEG_D);
      break;
  }//fin switch
}

/*
 * Fait avancer un serpent dessine sur les afficheurs grace aux etapes de la fonction serpent_etape().
 * La perdiode corresond au temps d'attente entre deux etapes du serpent
 */
void serpent(int periode){
  for (int i=0;i<=8;i++){
    serpent_etape(i);
    delay(periode);
  }
}
```

##Cinquième programme : lire les valeurs d'un potentiomètre sur un afficheur 7-segments
Ce programme a été écrit en collaboration avec **Doriane Galbez**.
Cette quatrième étape permet de lire les valeurs d'un potentiomètre sur un afficheur 7-segments.

Voici le schéma du montage :
![](../images/schema_montage_7seg_poto.JPG)

```
/*
 *  File        : prog2_7seg_poto.ino
 *  Authors     : Doriane Galbez
 *  Contributor : Kristine Valat
 *  Date        : 19/03/2021
 *  License     : This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 *
 * ----------------------------------------------------------------------------------------------------
 * Programme affichant successivement la valeur brute d'un potentiometre puis sa convertion en Volts.
 *  - potentiometre connecte sur le port A0
 *  - afficheurs 7 segments connectes sur les ports 2 a 13
 * ----------------------------------------------------------------------------------------------------
 *  
 *
 * Sur un afficheur 7 segments, les segments sont nommes de la maniere suivante :
 *       A
 *      ---
 *   F |   | B
 *     | G |
 *      ---
 *   E |   | C
 *     |   |
 *      ---  O DP
 *       D
 *
 *
 */


//definition des noms attribues aux ports
const int SEG_A = 12;    //segment correspondant au segment a
const int SEG_B = 8;     //segment correspondant au segment b
const int SEG_C = 4;     //segment correspondant au segment c
const int SEG_D = 5;     //segment correspondant au segment d
const int SEG_E = 6;     //segment correspondant au segment e
const int SEG_F = 11;    //segment correspondant au segment f
const int SEG_G = 3;     //segment correspondant au segment g
const int SEG_DP = 7;    //segment correspondant au segment du point
const int DIGIT_1 = 13;  //7 segment numero 1
const int DIGIT_2 = 10;  //7 segment numero 2
const int DIGIT_3 = 9;   //7 segment numero 3
const int DIGIT_4 = 2;   //7 segment numero 4

const int POTO = A0;


void setup() {
  //initialisation des ports
  pinMode(SEG_A,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(SEG_B,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(SEG_C,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(SEG_D,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(SEG_E,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(SEG_F,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(SEG_G,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(DIGIT_1,OUTPUT); //initialisation du port en tant que sortie
  pinMode(DIGIT_2,OUTPUT); //initialisation du port en tant que sortie
  pinMode(DIGIT_3,OUTPUT); //initialisation du port en tant que sortie
  pinMode(DIGIT_4,OUTPUT); //initialisation du port en tant que sortie

  pinMode(POTO,INPUT);    //initialisstion du port en tant qu'entree

}

void loop() {
  affiche_valeur_poto_brute(analogRead(POTO));
  delay(500);    //attente de 500 millisecondes soit 0,5 seconde
  affiche_valeur_poto_volts(analogRead(POTO));
  delay(500);    //attente de 500 millisecondes soit 0,5 seconde

}

/*
 * Remise a zero de tout les segments
 */
void eteint_tout(){
  /* option 1*/
  digitalWrite(DIGIT_1, LOW);
  digitalWrite(DIGIT_2, LOW);
  digitalWrite(DIGIT_3, LOW);
  digitalWrite(DIGIT_4, LOW);

  /* option 2*/
  /*
  digitalWrite(DIGIT_1, HIGH);
  digitalWrite(DIGIT_2, HIGH);
  digitalWrite(DIGIT_3, HIGH);
  digitalWrite(DIGIT_4, HIGH);
  digitalWrite(SEG_A, LOW);
  digitalWrite(SEG_B, LOW);
  digitalWrite(SEG_C, LOW);
  digitalWrite(SEG_D, LOW);
  digitalWrite(SEG_E, LOW);
  digitalWrite(SEG_F, LOW);
  digitalWrite(SEG_G, LOW);
  digitalWrite(SEG_DP, LOW);
  */
}

/*
 * Eteint un des afficheur 7 segments
 * - digit : (DIGIT_1,DIGIT_2,DIGIT_3 ou DIGIT_4) afficheur 7 segments qu'on veut eteindre
 */
void eteint_un_7seg(int digit){
  /*option 1*/
  digitalWrite(digit, LOW); //selection du 7 segments

  /*option 2*/
  /*digitalWrite(digit, LOW); //selection du 7 segments
  digitalWrite(SEG_A, LOW);
  digitalWrite(SEG_B, LOW);
  digitalWrite(SEG_C, LOW);
  digitalWrite(SEG_D, LOW);
  digitalWrite(SEG_E, LOW);
  digitalWrite(SEG_F, LOW);
  digitalWrite(SEG_G, LOW);
  digitalWrite(SEG_DP, LOW);
  */
}

/*
 * Eteint un segment sur un des afficheur 7 segments
 * - digit : (DIGIT_1,DIGIT_2,DIGIT_3 ou DIGIT_4) afficheur 7 segments qu'on veut utiliser
 * - seg   : (SEG_A,SEG_B,SEG_C,SEG_D,SEG_E,SEG_F,SEG_G ou SEG_DP) segment qu'on veut eteindre
 */
void eteint_un_seg(int digit, int seg){
  digitalWrite(digit, HIGH); //selection du 7 segments
  digitalWrite(seg, LOW);    //eteint un segment du 7 segments
}

/*
 * Allume un segment sur un des afficheur 7 segments
 * - digit : (DIGIT_1,DIGIT_2,DIGIT_3 ou DIGIT_4) afficheur 7 segments qu'on veut utiliser
 * - seg   : (SEG_A,SEG_B,SEG_C,SEG_D,SEG_E,SEG_F,SEG_G ou SEG_DP) segment qu'on veut allumer
 */
void affiche_un_seg(int digit, int seg){
  digitalWrite(digit, HIGH); //selection du 7 segments
  digitalWrite(seg, HIGH);   //allume un segment du 7 segments
}


/*
 * Allume les segments d'un des afficheur 7 segments pour former un chiffre
 * - digit   : (DIGIT_1,DIGIT_2,DIGIT_3 ou DIGIT_4) afficheur 7 segments qu'on veut utiliser
 * - chiffre : le chiffre (0,1,2,3,4,5,6,7,8,9)
 */
void convertion_chiffre_en_segment(int digit, int chiffre){
  eteint_un_7seg(digit); //eteint l'afficheur 7 segments concerne pour
  switch (chiffre){
    default:
      break;
    case 0:
      //           _
      //affiche : | |
      //          |_|
      affiche_un_seg(digit,SEG_A);
      affiche_un_seg(digit,SEG_B);
      affiche_un_seg(digit,SEG_C);
      affiche_un_seg(digit,SEG_D);
      affiche_un_seg(digit,SEG_E);
      affiche_un_seg(digit,SEG_F);
      break;
    case 1:
      //            
      //affiche :   |
      //            |
      affiche_un_seg(digit,SEG_B);
      affiche_un_seg(digit,SEG_C);
      break;
    case 2:
      //           _
      //affiche :  _|
      //          |_
      affiche_un_seg(digit,SEG_A);
      affiche_un_seg(digit,SEG_B);
      affiche_un_seg(digit,SEG_G);
      affiche_un_seg(digit,SEG_E);
      affiche_un_seg(digit,SEG_D);
      break;
    case 3:
      //           _
      //affiche :  _|
      //           _|
      affiche_un_seg(digit,SEG_A);
      affiche_un_seg(digit,SEG_B);
      affiche_un_seg(digit,SEG_G);
      affiche_un_seg(digit,SEG_C);
      affiche_un_seg(digit,SEG_D);
      break;
    case 4:
      //            
      //affiche : |_|
      //            |
      affiche_un_seg(digit,SEG_F);
      affiche_un_seg(digit,SEG_G);
      affiche_un_seg(digit,SEG_B);
      affiche_un_seg(digit,SEG_C);
      break;
    case 5:
      //           _
      //affiche : |_
      //           _|
      affiche_un_seg(digit,SEG_A);
      affiche_un_seg(digit,SEG_F);
      affiche_un_seg(digit,SEG_G);
      affiche_un_seg(digit,SEG_C);
      affiche_un_seg(digit,SEG_D);
      break;
    case 6:
      //           _
      //affiche : |_
      //          |_|
      affiche_un_seg(digit,SEG_A);
      affiche_un_seg(digit,SEG_F);
      affiche_un_seg(digit,SEG_G);
      affiche_un_seg(digit,SEG_C);
      affiche_un_seg(digit,SEG_D);
      affiche_un_seg(digit,SEG_E);
      break;
    case 7:
      //           _
      //affiche :   |
      //            |
      affiche_un_seg(digit,SEG_A);
      affiche_un_seg(digit,SEG_B);
      affiche_un_seg(digit,SEG_C);
      break;
    case 8:
      //           _
      //affiche : |_|
      //          |_|
      affiche_un_seg(digit,SEG_A);
      affiche_un_seg(digit,SEG_B);
      affiche_un_seg(digit,SEG_C);
      affiche_un_seg(digit,SEG_D);
      affiche_un_seg(digit,SEG_E);
      affiche_un_seg(digit,SEG_F);
      affiche_un_seg(digit,SEG_G);
      break;
    case 9:
      //           _
      //affiche : |_|
      //           _|
      affiche_un_seg(digit,SEG_A);
      affiche_un_seg(digit,SEG_B);
      affiche_un_seg(digit,SEG_C);
      affiche_un_seg(digit,SEG_D);
      affiche_un_seg(digit,SEG_F);
      affiche_un_seg(digit,SEG_G);
      break;

  }//fin switch
}



/*
 * Affiche sur les 4 afficheurs 7 segments la valeur brute lue sur le potentiometre.
 * La valeur brute est la valeur sans conversion, elle va de 0 à 1024.
 * - val : valeur (brute) lue sur le potentiometre
 */
void affiche_valeur_poto_brute(int val){
  float tmp = 0.0;
  tmp = (float)val/1000;
  convertion_chiffre_en_segment(DIGIT_1, (int)tmp); //extrait les miliers
  tmp = tmp-(int)tmp;
  tmp = tmp*10;
  convertion_chiffre_en_segment(DIGIT_2, (int)tmp); //extrait les centaines
  tmp = tmp-(int)tmp;
  tmp = tmp*10;
  convertion_chiffre_en_segment(DIGIT_3, (int)tmp); //extrait les dizaines
  tmp = tmp-(int)tmp;
  tmp = tmp*10;
  convertion_chiffre_en_segment(DIGIT_4, (int)tmp); //extrait les unites
}


/*
 * Affiche sur les 4 afficheurs 7 segments la valeur convertie en Volts lue sur le potentiometre.
 * La valeur brute maximum est 1024, elle correspond à 5V. Pour trouver la valeur en Volts on peut simplement
 * faire un produit en croix tel que valeur_en_volts=valeur_brute_lue*5/1024.
 * - val : valeur (brute) lue sur le potentiometre
 */
void affiche_valeur_poto_volts(int val){
  float tmp = (float)val*5/1024;  //convertion de la donnee du potentiometre en Volts
  convertion_chiffre_en_segment(DIGIT_1, (int)tmp); //extrait les miliers
  affiche_un_seg(DIGIT_1,SEG_DP);  //affiche la virgule
  tmp = tmp-(int)tmp;
  tmp = tmp*10;
  convertion_chiffre_en_segment(DIGIT_2, (int)tmp); //extrait les centaines
  tmp = tmp-(int)tmp;
  tmp = tmp*10;
  convertion_chiffre_en_segment(DIGIT_3, (int)tmp); //extrait les dizaines
  tmp = tmp-(int)tmp;
  tmp = tmp*10;
  convertion_chiffre_en_segment(DIGIT_4, (int)tmp); //extrait les unites
}
```

##Problèmes rencontrés
Nous avons eu un petit problème avec l'afficheur 7-segments, l'intensité de chaque afficheur n'était pas la même mais pas tout le temps. En vérifiant la datasheet, nous avons vu qu'il fallait une résitance par digit pour éviter ce problème.
