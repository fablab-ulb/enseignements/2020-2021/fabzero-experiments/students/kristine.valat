# 4. Découpe assistée par ordinateur

Lors de cette partie, nous avons travaillé sur 2 découpeuses : la découpeuse laser et la découpeuse vinyle. Chacune des découpeuses peut graver et découper. Pour chacune des découpeuses, il est conseillé de faire des tests avant de découper la forme que nous voulons pour éviter les mauvaises surprises ainsi que la perte de matériaux.

##Découpeuse laser
Avant de commencer à faire tout modèle sur la découpeuse, nous avons d'abord fait une matrice des puissances en fonction des vitesses pour savoir la puissance et la vitesse qu'il faut mettre en fonction du résultat que l'on veut obtenir (grosses coupures ou coupures plus ou moins superficielles). Nous avons réalisé cette matrice sur **Inkscape** pour le papier que nous souhaitions découper.

![](../images/decoupe_laser.PNG)
[Voici le fichier Inkscape utilisé](../images/calibration_decoupe_laser.svg)

Chaque couleur correspond à une certaine vitesse et puissance. ensuite, nous avons paramètré cela sur la découpeuse laser.

![](../images/rendulaser.JPG)

Ensuite, j'ai créé un kirigami qui aura cette apparence :
![](../images/modele.PNG)

[Voici le fichier que j'ai alors créé sur Inkscape .svg](../images/rendu.svg)

Voici le résultat du kirigami (même si on ne voit pas bien le résultat sur la photo ci-dessous) :

![](../images/laser.JPG)

##Découpeuse Vinyle
Cette partie a été réalisée avec Doriane Galbez.
Avant de commencer à faire tout modèle sur la découpeuse, nous avons d'abord fait quelques découpes afin de savoir la puissance et la vitesse à mettre en fonction du résultat que l'on veut obtenir (grosses coupures ou coupures plus ou moins superficielles). Nous avons réalisé cela sur **Inkscape** pour le papier vinyle que nous souhaitions découper.

Ce logo est un des logos proposés pour le club Strat' de l'ESIEE Paris.

![](../images/logo_strat.JPG)

Nous avons d'abord isolé les contours du logo afin de pouvoir le découper à l'aide de la découpeuse vinyle Silhouette. Pour l'utiliser, nous avons d'abord télécharger le logiciel associé et importé le fichier au format dxf.

![](../images/image_logo_strat.JPG)

Nous avons mis 2 couleurs (rouge et noire) pour 2 découpes différentes. Le rouge sera juste de graver donc une découpe fine et la découpe noire est une découpe plus profonde du papier vinyle. Nous avons comme paramètre : 20 en vitesse et 4 en puissance.

![](../images/silouhette_studio.JPG)

Il y avait trop de détails sur la photo qui sont partis lorsque j'ai essayé de décoller le sticker. J'ai collé ce sticker sur mon ordinateur portable et en voici le résultat :

![](../images/logo_strat_decoupe.JPG)
