# 6. Electronique 2 - Fabrication
Nous avons assemblé un circuit qui aura le même principe qu'une carte Arduino. Nous pourrons donc connecter des composants et écrire des programmes légers puisque la carte a peu d'espace mémoire.

##Soudure
La soudure permet de relier un composant à un circuit imprimé, la soudure est réalisée à l'aide d'un fer à souder et de l'étain. Pour les petits composants, il faut utiliser une pince. Il est parfois utile de mettre de l'étain sur les bords des composants avant de les mettre sur la carte. Le fer à souder chauffe beaucoup, il est donc nécessaire de ne pas laisser le fer en contact trop longtemps avec certains composants qui ne supportent pas beaucoup la chaleur. Si une soudure est ratée, il est possible de refaire fondre l'étain pour défaire la soudure.

Une fois la carte en main, il faut vérifier que les pistes (parties de cuivre sur la carte) soient bien séparées selon les contours désirés et qu'il n'y ait pas de cuivre qui relie deux pistes par erreurs.

Voici l'image du PCB (circuit imprimé) avant les soudures :
![](../images/PCB.JPG)

L'ordre de soudure a été le suivant (les soudures que j'ai effectué ne sont pas parfaites) :
* Microprocesseur
![](../images/microprocesseur.JPG)

* Régulateur de tension
![](../images/regulateur.JPG)
Une fois cette étape réalisée, la carte peut être connectée sur un ordinateur. Pour vérifier que la carte fonctionne, il suffit de la connecter à un ordinateur et de regarder dans le gestionnaire de périphérique qu'il y ait le port COM et LPT et que la carte est bien connectée.
![](../images/PCB1.PNG)

* LED
![](../images/LED.JPG)

* Derniers composants (boutons, 2ème LED ...)
![](../images/carte.JPG)

##Programmes associés
Nous pouvons regarder que les composants ont bien été soudé et sont maintenant utilisables. Les programmes ont été réalisé entre les soudures pour savoir si c'est que l'on vient de souder qui a un problème.

Lors de la connexion de la carte, nous devons configurer sur Arduino IDE le fait que ce sera notre carte que nous prendrons en compte.

Il faut aller dans Fichier puis Préférences et ajouter l’URL “https://www.mattairtech.com/software/arduino/package_MattairTech_index.json”.
![](../images/Arduino.PNG)

Puis il faut aller dans Outils ensuite Type de carte enfin Gestionnaire de cartes et tapper “mattair” dans la barre de recherche et l'installer.
![](../images/arduinoIDE.PNG)

A l'aide du tableau ci-dessous, nous pouvons savoir sur quel port est connecté un composant ce qui nous aidera pour écrire les programmes.
![](../images/pin.PNG)

###Programme : allumer une LED
Le premier programme est après la soudure de la première LED pour vérifier son fonctionnement.

```
/*
  FILE   : LED_PCB.ino

  AUTHOR : Kristine VALAT <kristine.valat@edu.esiee.fr>

  DATE   : 2021-27-04

  LICENSE : Attribution 4.0 International [CC BY 4.0] (https://creativecommons.org/licenses/by/4.0/)

  Ce programme fait clignoter une LED
*/

const int LED = 15; //Initialisation de la LED sur le port 15

void setup() {
  // initialisation de la LED en tant que sortie
  pinMode(LED, OUTPUT);
}

// boucle qui sera exécutée (équivalent du while(1)) la boucle ne s'arrete pas tant que l'on appuie pas sur reset
void loop() {
  digitalWrite(LED, HIGH);   //Allume la LED
  delay(1000);               //Attend 1 000 ms soit 1 s
  digitalWrite(LED, LOW);    //Eteint la LED
  delay(1000);               //Attend 1 000 ms soit 1 s
}
```
Avec ce programme, on voit bien que la LED verte fonctionne.

Ensuite, nous vérifions que la deuxième LED (rouge) fonctionne lorsque l'on connecte la carte àun ordinateur.
![](../images/LEDs.JPG)

###Programme : allumer une LED avec un boutons
Le but de ce programme est d'allumer la LED verte avec un bouton.

```
/*
  FILE   : Bouton_LED.ino

  AUTHOR : Kristine VALAT <kristine.valat@edu.esiee.fr>

  DATE   : 2021-27-04

  LICENSE : Attribution 4.0 International [CC BY 4.0] (https://creativecommons.org/licenses/by/4.0/)

  Ce programme fait clignoter une LED
*/

const int LED = 15; //Initialisation de la LED sur le port 15
const int Bouton = 8; //Initialisation de la LED sur le port 8

void setup() {
  // initialisation de la LED en tant que sortie
  pinMode(LED, OUTPUT);
  pinMode(Bouton, INPUT);
}

void loop() {
  if(digitalRead(Bouton)==0)     //lit la valeur du bouton (0 ou 1)
    digitalWrite(LED, HIGH);   // allume la led
  else
    digitalWrite(LED, LOW);    // eteint la led
}
```
##Problèmes rencontrés
Lorsque l'on touche le connecteur relié au bouton la LED verte s'allume quand même puisque le connecteur fait office d'antenne. Pour résoudre ce problème, il suffit de connecter ce connecteur à un composant.
