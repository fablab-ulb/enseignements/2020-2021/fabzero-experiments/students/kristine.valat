#Impression 3D
Cette partie est consacrée à l'impression 3D. Afin d'imprimer une pièce, j'utilise le logiciel PrusaSlicer. Ce logiciel nous permet de voir dans le plan l'objet que nous souhaitons imprimer. Pour pouvoir imprimer sur les imprimantes 3D sans problème, il faut d'abord bien configurer le logiciel avec l'imprimante que nous avons (nous utilisons l'imprimante Original Prusa i3 MK3S & MK3S+). Une fois l'imprimante configurée et que le modèle apparait dans les bonnes dimensions sur le logiciel (et éventuellement changer l'épaisseur du support ou de la jupe), nous pouvons imprimer. Pour cela, le logiciel affiche le temps d'impression et créer un fichier **.gcode**. Il suffit ensuite de mettre le fichier sur la carte SD de l'imprimante et de lancer l'impression.

##Test sur l'imprimante
Durant les tests de l'imprimante 3D je n'ai pas eu de problème, j'ai appris qu'il valait mieux mettre toujours un support quand on imprimait des pièces surtout si elles sont importantes. Mais j'ai pu observer des problèmes sur d'autres imprimantes :

 * **Problème de fil** : il ne faut pas lancer une impression s'il n'y a pas assez de fil dans l'imprimante donc il faut toujours vérifier qu'il y en a suffisament pour imprimer sa pièce pour ne pas avoir d'interruption lorsqu'on imprime sinon il faut recommencer.

 * **Problème durant l'impression** : il faut rester devant l'imprimante pendant l'impression (au moins au début) afin de vérifier que les pièces s'impriment bien et qu'il n'y a pas une erreur lors du dépôt d'une couche. Si ce problème survient l'imprimante peut imprimer dans le vide, dans ce cas il faut arrêter l'impression.

##Kit complet
Avec les pièces qui sont dans la partie **2.Conception Assistée par Ordinateur**, j'ai pu créer un kit en ajoutant quelques pièces créées par [Doriane Galbez](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/). J'ai donc imprimé le tout afin d'avoir un kit composé de cinq différents FlexLinks. De plus, j'ai ajouté des crédits sur mon travail et j'ai laissé les crédits sur le travail de ma camarade. Pour faire mes crédits, j'ai utilisé la licence [Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/) et j'ai ajouté au début de mon code la partie suivante :

```
FILE   : flex1.scad

AUTHOR : Kristine VALAT <kristine.valat@edu.esiee.fr>

DATE   : 2021-03-08

LICENSE : Attribution 4.0 International [CC BY 4.0] (https://creativecommons.org/licenses/by/4.0/)

CONTRIBUTORS :
  - Doriane GALBEZ <doriane.galbez@edu.esiee.fr> : corrections de bug
```

![](../images/kit.JPG)

Dans ce kit on retrouve donc :

* [**Fixed-Fixed Beam**](https://www.thingiverse.com/thing:3020736) : que j'ai créée dans lequel [Doriane Galbez](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/) a effectué des petites corrections sur le code

  * [Télécharger la pièce au format .scad](../images/flex1.scad)
  * [Télécharger la pièce au format .stl](../images/flex1.stl)

![](../images/fixed.JPG)

* [**Fixed-Slotted Beam - Straight**](https://www.thingiverse.com/thing:3027490) : que j'ai créée dans lequel [Doriane Galbez](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/) a effectué des petites corrections sur le code

  * [Télécharger la pièce au format .scad](../images/flex2.scad)
  * [Télécharger la pièce au format .stl](../images/flex2.stl)

![](../images/slotted.JPG)

* [**Canteliver Beam**](https://static.wixstatic.com/media/f04fcf_2a905e982b8c4c248b829911bfedcb73~mv2.jpg/v1/fill/w_460,h_345,al_c,q_80,usm_0.66_1.00_0.01/f04fcf_2a905e982b8c4c248b829911bfedcb73~mv2.jpg) : que j'ai créée dans lequel [Doriane Galbez](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/) a effectué des petites corrections sur le code

  * [Télécharger la pièce au format .scad](../images/flex3.scad)
  * [Télécharger la pièce au format .stl](../images/flex3.stl)

![](../images/canteliver.JPG)

* [**Fixed-Fixed Beam - Initially Curved**](https://www.thingiverse.com/thing:3016949) : créée par [Doriane Galbez](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/), je n'ai pas modifié le code de pièce car il est déjà paramétrable

  * [Télécharger la pièce au format .scad](../images/flex4.scad)
  * [Télécharger la pièce au format .stl](../images/flex4.stl)

![](../images/curved.JPG)

* **Satellite** : créée par [Doriane Galbez](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/), je n'ai pas modifié le code de pièce car il est déjà paramétrable

  * [Télécharger la pièce au format .scad](../images/flex5.scad)
  * [Télécharger la pièce au format .stl](../images/flex5.stl)

![](../images/satellite.JPG)

##Mécanisme avec des FlexLink
Le mécanisme que j'ai voulu reproduire est celui du [Bistable Compliant Switch](https://www.thingiverse.com/thing:2988576). Pour le faire en utilisant que des FlexLinks, le système est légèrement simplifié.

 [Vidéo](../images/meca.mp4)
