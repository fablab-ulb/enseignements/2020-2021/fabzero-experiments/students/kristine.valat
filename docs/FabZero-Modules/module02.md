#Conception Assistée par Ordinateur

Le but de cette partie est de créer différents Flexlinks que nous pourrons imprimer grâce aux imprimantes 3D. Tout d'abord, j'ai choisi sur [_BYU CMR_](https://www.compliantmechanisms.byu.edu/flexlinks) plusieurs FlexLinks que je pourrai réaliser. Ensuite, nous avions le choix entre le logiciel OpenSCAD ou FreeCAD. Le logiciel OpenSCAD me semblait plus adapté car il est facile de modifier rapidement une pièce en la rendant paramétrable.

OpenSCAD permet de créer des modèles à l'aide de forme (rectangles, cercles..), les modèles que nous pouvons créer doivent donc être décrit en ajoutant et soustrayant des formes.

##Fixed-Fixed Beam
###La pièce
Pour le premier FlexLink que j'ai fait, j'ai voulu partir sur un modèle assez simple ([FlexLink 1](https://www.thingiverse.com/thing:3020736)).

Voici donc ce que la pièce donne une fois réalisé sur OpenSCAD :
![](../images/flex1.PNG)

###Le code
J'ai ainsi commencé par faire une extrémité de la pièce qui est composée d'un carré et de deux cercles afin d'arrondir les bords pour que cela ressemble le plus possible au modèle. Ensuite, j'ai ajouté la barre principale du milieu. Puis, j'ai créé la deuxième extrémité de la même façon que la première en la décalant. Pour finir, j'ai fait les différents trous de la pièce.

Si vous voulez télécharger le code de ma pièce [Flex 1](../images/flex1.scad).
Le code est disponible ci-dessous :
```
/*
FILE   : flex1.scad

AUTHOR : Kristine VALAT <kristine.valat@edu.esiee.fr>

DATE   : 2021-03-08

LICENSE : Attribution 4.0 International [CC BY 4.0] (https://creativecommons.org/licenses/by/4.0/)

CONTRIBUTORS :
  - Doriane GALBEZ <doriane.galbez@edu.esiee.fr> : corrections de bug
*/
$fn= 50;
epsilon = 0.01;
//ce que l'on peut changer
    //pièce carré arrondie
depth = 0.7; //largeur
height = 0.6; //hauteur

    //trou de la pièce
bordure = 0.1; //espace entre la bordure et le trou
longueur = 0.8; // longueur entre 2 trous

    //barre du milieu
length_mil = 8;
depth_mil = 0.1;

//paramètres fixés
    //pièce carré arrondie
diametre = depth;
rayon = depth/2;
rayon2 = rayon-bordure; //rayon des trous
length = longueur + bordure *2 ;



difference(){
    // partie cube
    union(){
        //partie barre du milieu   
        translate([0,depth/2,height/2])     //on a centré le cube
        cube([length_mil,depth_mil,height],center = true);

        // partie cube1
        translate([(length_mil/2 + diametre),depth/2,height/2])
        cube([longueur,depth,height],center =true);
        translate([length_mil/2+rayon,rayon,height/2]) // bordure *2 pour la confiance
        cylinder(height,rayon,rayon,center = true);
        translate([length_mil/2+rayon+longueur,depth/2,height/2])
        cylinder(height,rayon,rayon,center = true);



    // partie cube2
        translate([-(length_mil/2 + diametre),depth/2,height/2])
        cube([longueur,depth,height],center=true);
        translate([-(length_mil/2+rayon),depth/2,height/2])
        cylinder(height,rayon,rayon,center = true);
        translate([-(length_mil/2+rayon+longueur),depth/2,height/2])
        cylinder(height,rayon,rayon,center = true);
    }

    // partie trous
    translate([length_mil/2 + rayon,rayon,-epsilon/2])
    cylinder(height+epsilon,rayon2,rayon2);
    translate([length_mil/2 +rayon+longueur,rayon,-epsilon/2])
    cylinder(height+epsilon,rayon2,rayon2);

    translate([-(length_mil/2 + rayon),rayon,-epsilon/2])
    cylinder(height+epsilon,rayon2,rayon2);
    translate([-(length_mil/2 +rayon+longueur),rayon,-epsilon/2])
    cylinder(height+epsilon,rayon2,rayon2);
}
```

Comme nous pouvons le voir, j'ai mis des paramètres que nous pouvons changer et ceux qui changent automatiquement. Les différents paramètres sont décrits ci-dessous :
![](../images/flex1_modele.PNG)

##Fixed-Slotted Beam - Straight
###La pièce
Pour le deuxième FlexLink, j'ai voulu reprendre une partie du dernier modèle ([FlexLink 2](https://www.thingiverse.com/thing:3027490)).

Voici donc ce que la pièce donne une fois réalisé sur OpenSCAD :
![](../images/flex2.PNG)

###Le code
J'ai ainsi commencé par faire une extrémité de la pièce qui est composée d'un carré et de deux cercles afin d'arrondir les bords pour que cela ressemble le plus possible au modèle. Ensuite, j'ai ajouté la barre principale du milieu. Puis, j'ai créé la deuxième extrémité de la même façon que la première en la décalant. Pour finir, j'ai fait les différents trous de la pièce en rajoutant pour la deuxième partie un carré pour que le trou soit semblable au modèle.

Si vous voulez télécharger le code ma pièce [Flex 2](../images/flex2.scad).
Le code est disponible ci-dessous :
```
/*
FILE   : flex2.scad

AUTHOR : Kristine VALAT <kristine.valat@edu.esiee.fr>

DATE   : 2021-03-08

LICENSE : Attribution 4.0 International [CC BY 4.0] (https://creativecommons.org/licenses/by/4.0/)

CONTRIBUTORS :
  - Doriane GALBEZ <doriane.galbez@edu.esiee.fr> : corrections de bug
*/

$fn= 50;
epsilon = 0.01;
//ce que l'on peut changer
    //pièce carré arrondie
depth = 0.7; //largeur 0.7
height = 0.6; //hauteur 0.6

    //trou de la pièce
bordure = 0.1; //espace entre la bordure et le trou 0.1
longueur = 0.8; // longueur entre 2 trous 0.8

    //barre du milieu
length_mil = 8; //8
depth_mil = 0.1; //0.1

//paramètres fixés
    //pièce carré arrondie
diametre = depth;
rayon = depth/2;
rayon2 = rayon-bordure; //rayon des trous
length = longueur + bordure *2 ; //longueur 0.8



difference(){
    // partie cube
    union(){
        //partie barre du milieu   
        translate([0,depth/2,height/2])     //on a centré le cube
        cube([length_mil,depth_mil,height],center = true);

        // partie cube1
        translate([(length_mil/2 + diametre),depth/2,height/2])
        cube([longueur,depth,height],center =true);
        translate([length_mil/2+rayon,rayon,height/2]) // bordure *2 pour la confiance
        cylinder(height,rayon,rayon,center = true);
        translate([length_mil/2+rayon+longueur,depth/2,height/2])
        cylinder(height,rayon,rayon,center = true);



    // partie cube2
        translate([-(length_mil/2 + diametre),depth/2,height/2])
        cube([longueur,depth,height],center=true);
        translate([-(length_mil/2+rayon),depth/2,height/2])
        cylinder(height,rayon,rayon,center = true);
        translate([-(length_mil/2+rayon+longueur),depth/2,height/2])
        cylinder(height,rayon,rayon,center = true);
    }

    // partie trous
    translate([length_mil/2 + rayon,rayon,-epsilon/2])
    cylinder(height+epsilon,rayon2,rayon2);
    translate([length_mil/2 +rayon+longueur,rayon,-epsilon/2])
    cylinder(height+epsilon,rayon2,rayon2);

    translate([-(length_mil/2 + rayon),rayon,-epsilon/2])
    cylinder(height+epsilon,rayon2,rayon2);
    translate([-(length_mil/2 +rayon+longueur),rayon,-epsilon/2])
    cylinder(height+epsilon,rayon2,rayon2);
    translate([-(length_mil/2 +diametre),depth/2,height/2])
    cube([longueur-bordure,depth-bordure*2,height+epsilon/2],center=true);
}
```

Comme nous pouvons le voir, j'ai mis des paramètres que nous pouvons changer et ceux qui changent automatiquement. Les différents paramètres sont décrits ci-dessous :
![](../images/flex2_modele.PNG)

##Canteliver Beam
###La pièce
Pour ce FlexLink, j'ai repris le premier FlexLink puisque j'ai perdu beaucoup de temps en correction d'erreur ([FlexLink 3](https://static.wixstatic.com/media/f04fcf_2a905e982b8c4c248b829911bfedcb73~mv2.jpg/v1/fill/w_460,h_345,al_c,q_80,usm_0.66_1.00_0.01/f04fcf_2a905e982b8c4c248b829911bfedcb73~mv2.jpg)).

Voici donc ce que la pièce donne une fois réalisé sur OpenSCAD :
![](../images/flex3.PNG)

###Le code
J'ai ainsi commencé par faire une extrémité de la pièce qui est composée d'un carré et de deux cercles afin d'arrondir les bords pour que cela ressemble le plus possible au modèle. Ensuite, j'ai ajouté la barre principale du milieu. Pour finir, j'ai fait les différents trous de la pièce.

Si vous voulez télécharger le code ma pièce [Flex 3](../images/flex3.scad).
Le code est disponible ci-dessous :
```
/*
FILE   : flex3.scad

AUTHOR : Kristine VALAT <kristine.valat@edu.esiee.fr>

DATE   : 2021-03-08

LICENSE : Attribution 4.0 International [CC BY 4.0] (https://creativecommons.org/licenses/by/4.0/)

CONTRIBUTORS :
  - Doriane GALBEZ <doriane.galbez@edu.esiee.fr> : corrections de bug
*/

$fn= 50;
epsilon = 0.01;
//ce que l'on peut changer
    //pièce carré arrondie
depth = 0.7; //largeur 0.7
height = 0.6; //hauteur 0.6

    //trou de la pièce
bordure = 0.1; //espace entre la bordure et le trou 0.1
longueur = 0.8; // longueur entre 2 trous 0.8

    //barre du milieu
length_mil = 8; //8
depth_mil = 0.1; //0.1

//paramètres fixés
    //pièce carré arrondie
diametre = depth;
rayon = depth/2;
rayon2 = rayon-bordure; //rayon des trous
length = longueur + bordure *2 ; //longueur 0.8



difference(){
    // partie cube
    union(){
        //partie barre du milieu   
        translate([0,depth/2,height/2])     //on a centré le cube
        cube([length_mil,depth_mil,height],center = true);

        // partie cube1
        translate([(length_mil/2 + diametre),depth/2,height/2])
        cube([longueur,depth,height],center =true);
        translate([length_mil/2+rayon,rayon,height/2]) // bordure *2 pour la confiance
        cylinder(height,rayon,rayon,center = true);
        translate([length_mil/2+rayon+longueur,depth/2,height/2])
        cylinder(height,rayon,rayon,center = true);
    }

    // partie trous
    translate([length_mil/2 + rayon,rayon,-epsilon/2])
    cylinder(height+epsilon,rayon2,rayon2);
    translate([length_mil/2 +rayon+longueur,rayon,-epsilon/2])
    cylinder(height+epsilon,rayon2,rayon2);

}
```
Comme nous pouvons le voir, j'ai mis des paramètres que nous pouvons changer et ceux qui changent automatiquement. Les différents paramètres sont décrits ci-dessous :
![](../images/flex3_modele.PNG)


##Problèmes rencontrés
En effet, les trois modèles que j'ai réalisé se ressemblent car j'ai pris beaucoup de temps pour corriger de nombreux problèmes que j'ai rencontré.

###Problème de code / changement de dimension
Mon premier problème était celui du paramètrage, lorsque que je changeais un paramètre tout le modèle se déformait et certains éléments n'étaient plus en place. Pour pallier ce problème, j'ai d'abord essayé de réécrire le code en repartant à zéro. Même après deux essais le problème persistait, j'ai donc décidé de décaler le modèle dans le plan et de centrer toute la pièce sur le milieu du plan. Mais le problème persiste, j'ai donc montré mon code à [Doriane Galbez](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/) qui a corrigé quelques erreurs or j'ai toujours le même problème. Si on change certains paramètres sans changer la plupart des autres, il se peut que des éléments soient mal placés.

###Problème après impression
Lors du passage de OpenSCAD à PrusaSlicer (le logiciel que nous utilsons pour générer le gcode à imprimer), il y avait un problème d'unité. En effet, cela convertissait toutes mes valeurs en pouces, j'ai donc changé les valeurs avant impression. Puis j'ai trouvé sur PrusaSlicer comment faire pour que les valeurs soient finalement en centimètre. Et j'ai eu un problème d'un paramètre qui était mal calibré. J'ai changé cela dans le code et j'ai trouvé comment faire pour que les valeurs ne soient plus converties en pouce lors qu'un passage d'un logiciel à l'autre.  
