#Documentation
##Objectifs
Le but de ce cours est de mettre en forme et à jour les pages de ce site. Pour cela, j'ai installé les logiciels sur mon ordinateur  (qui est sous Windosw 10) **Atom** ainsi que le package **markdown-preview-enhanced** et **Git**.
**Atom** permet d'écrire et de visualiser le rendu du code écrit quasiment simultanément.
Les pages de ce site seront écrites grâce au langage **Markdown**.


##Utilisation de Git
Il faut effectuer plusieurs étapes afin de pouvoir utiliser Git et changer les pages du site :

* Création d'une clé ssh
* Clonage du projet
* Ajout / Modification

Ces étapes ne sont pas très longues si nous savons comment les faire facilement mais elles sont nécessaires.

###Création d'une clé ssh
Tout d'abord, il nous faut une clé ssh pour faire la connexion entre le serveur Git et mon ordinateur. Le terminal Git Bash (obtenu après l'installation de Git sur mon ordinateur) et un terminal proche de celui de Linux.

_**Attention dans les différentes lignes de code suivantes, tout ce qui sera écrit entre des étoiles (*) sont des noms spécifiques que j'ai choisi et que l'on peut modifier.**_

Je me suis placée dans un répertoire spécialement créé pour la création de la clé grâce à la commande:

```
$cd *ssh*
```

Une fois dans le répertoire, il suffit de créer la clé :
```
$ssh-keygen
```

Nous obtenons alors :
![](../images/cle_ssh.PNG)

* **Enter file in which to save the key** : le fichier dans lequel sera la clé (j'ai choisi de nommé ce fichier *git*)
* **Enter passphrase** : la phrase qui nous servira de mot de passe (cette étape n'est pas obligatoire mais conseillée)
Lorsque l'on écrit dans **Enter passphrase**, on ne voit pas ce quel'on écrit et c'est normal (attention aux fautes de frappe).

J'ai donc deux nouveaux fichiers *git* et *git_pub*. La clé peut être récupérée grâce à la commande :
```
$cat *git.pub*
```

Une fois, la clé récupérée, il faut l'ajouter sur le git. Pour moi, il y avait un bouton en rouge quand je me connectais sur Git qui me demandait d'ajouter une clé ssh. J'ai donc pu l'ajouter très rapidement.

###Clonage du projet
Ensuite, il suffit de cloner le projet. Le projet a cloner est celui à son nom. Pour cela, j'ai récupéré le lien https lorsque l'on clique sur **"Clone"**. Puis on retourne dans le terminal **Git Bash** et on écrit :
```
$git clone *https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/kristine.valat.git*
```

![](../images/clone.PNG)


Avant d'ajouter des fichiers, j'ai dû me connecter à mon adresse mail utilisée pour la création de mon compte car il n'arrivait pas à détecter mon adresse mail automatiquement :
```
$git config --global user.email *adresse@mail.com*
```
![](../images/config.PNG)

###Ajout / Modification de fichier
Pour ajouter ou modifier (cela fait une mise à jour du fichier, il faut l'avoir modifié à l'avance) un fichier, il suffit d'utiliser la même commande :
```
$git add *nom_fichier1* *nom_fichier2*
```
Note : Il est possible d'ajouter plusieurs fichiers en une ligne

Pour enregistrer nos modifications :
```
$ git commit -m *"Description"*
```
Note : **-m** sert à ajouter une description lorsqu'on met à jour un fichier. Cela est conseillé pour avoir une trace de ce qui est modifié.

Pour regarder, les fichiers qui ont été modifié et ajouté :
```
$git status
```

Pour que les modifications soient prises en compte la prochaine fois que le site est mis à jour sur git, il faut faire la commande suivante :
```
$ git push origin master
```

Vos modifications seront donc prises en compte à la prochaine mise à jour du site.  
![](../images/modification.PNG)
